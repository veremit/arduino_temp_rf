// Duemilanove temp sense - RF socket controller

// include the library code:
#include <OneWire.h>
#include <LiquidCrystal.h>
#include <DallasTemperature.h>

#define		LED		13

// Data wire is plugged into pin 2 on the Arduino
#define 	ONE_WIRE_BUS 	10

// RF module
#define		RF_VCC			9
#define		RF_DATA			8
//#define		RF_GND			5

#define		LED2			11
#define		LED3			12

// Temp thresholds
#define		TEMP_OFF		19.7
#define		TEMP_ON			19.3

static const char _bits[8] = {41, 39, 37, 35, 33, 31, 29, 27};
static byte solidblk[8] = {
  B00000,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B00000,
};
static byte emptyblk[8] = {
  B00000,
  B11111,
  B10001,
  B10001,
  B10001,
  B11111,
  B00000,
};

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);

DeviceAddress temps[8];
char validadd[8];

void setup() {
  pinMode(RF_VCC, OUTPUT);
//  pinMode(RF_GND, OUTPUT);
  pinMode(RF_DATA, OUTPUT);

  pinMode(LED, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);

  Serial.begin(115200);
  Serial.println("Init");
  Serial.println("Testing RF sockets!");
  Serial.println("Dallas Temperature IC Control Library Demo");

  // Start up the library
  sensors.begin();
  // IC Default 9 bit. If you have troubles consider upping it 12.
  // Ups the delay giving the IC more time to process the temperature measurement

    // locate devices on the bus
  Serial.print("Locating devices...");
  Serial.print("Found ");
  char tempcount = sensors.getDeviceCount();
  Serial.print(tempcount, DEC);
  Serial.println(" devices.");
  for (char i=0; i<tempcount; i++)
  {
	//uint8_t *tmpadd;
	if (sensors.getAddress(temps[i], i))
	{
		validadd[i] = (char)TRUE;
		Serial.print("Sensor ");
		Serial.print(i+1, DEC);
		Serial.print(" address ");
		printAddress(temps[i]);
		//Serial.print(" contents ");
		Serial.println();
	}
	else
	{
		validadd[i] = (char)FALSE;
	}
  }
  
  // report parasite power requirements
  Serial.print("Parasite power is: ");
  if (sensors.isParasitePowerMode()) Serial.println("ON");
  else Serial.println("OFF");

//  digitalWrite(RF_GND, LOW);
}

// function to print a device address
void printAddress(DeviceAddress deviceAddress)
{
  for (uint8_t i = 0; i < 8; i++)
  {
    // zero pad the address if necessary
    if (deviceAddress[i] < 16) Serial.print("0");
    Serial.print(deviceAddress[i], HEX);
  }
}

/*
void initLCD() {
	delay(40);
	sendChar(0x3C, 0);
	sendChar(0x0E, 0);
	sendChar(0x01, 0);
	delay(2);
	sendChar(0x06, 0);
}

void sendChar(uint8_t value, uint8_t mode)
{
	if (mode) {
		digitalWrite(_rs, HIGH);
	} else {
		digitalWrite(_rs, LOW);
	}
	digitalWrite(_enable, LOW);
	delayMicroseconds(1);
	for (char i=0; i<8; i++)
	{
	    digitalWrite(_bits[i], (value >> i) & 0x01);
	}
	digitalWrite(_enable, HIGH);
	delayMicroseconds(1);    // enable pulse must be >450ns
	digitalWrite(_enable, LOW);
	delayMicroseconds(100);   // commands need > 37us to settle
}
*/

void ShortLong()
{
	digitalWrite(RF_DATA, HIGH);
	delayMicroseconds(190);
	digitalWrite(RF_DATA, LOW);
	delayMicroseconds(570);
}

void LongShort()
{
	digitalWrite(RF_DATA, HIGH);
	delayMicroseconds(570);
	digitalWrite(RF_DATA, LOW);
	delayMicroseconds(190);
}

void Send0()
{
	ShortLong();
	ShortLong();
}

void Send1()
{
	LongShort();
	LongShort();
}

void SendF()
{
	ShortLong();
	LongShort();
}

void DoSync()
{
	ShortLong();
	delayMicroseconds(770);
}

void sendID()
{
  SendF();
  SendF();
  SendF();
  Send0();
  Send0(); // remote ID
}

void chan1()
{
  SendF();
  SendF();
  SendF();
  Send0();
  Send1(); // chan 1
}

void chan2()
{
  SendF();
  SendF();
  SendF();
  Send1(); // chan 2
  Send0();
}

void chan3()
{
  SendF();
  SendF();
  Send1(); // chan 3
  Send0();
  Send0();
}

void chan4()
{
  SendF();
  Send1(); // chan 4
  SendF();
  Send0();
  Send0();
}

void chan5()
{
  Send1(); // chan 5
  SendF();
  SendF();
  Send0();
  Send0();
}

void SendOn()
{
  Send0();
  Send1(); // ON
}

void SendOff()
{
  Send1(); // OFF
  Send0();
}

unsigned long saveTime, timeTaken;

void loop() {
  saveTime=millis();
  digitalWrite(LED, HIGH);   // turn the LED on (HIGH is the voltage level)

  digitalWrite(LED3, HIGH);
  // call sensors.requestTemperatures() to issue a global temperature
  // request to all devices on the bus
  Serial.print("Requesting temperatures...");
  sensors.requestTemperaturesByIndex(0); // Send the command to get temperatures
  Serial.println("DONE");

  Serial.print("Temp 1 is: ");
  float temp1 = sensors.getTempC(temps[0]);
  Serial.println(temp1);
  //lcd.setCursor(7, 0);
  //lcd.print(temp1);
  
  //Serial.print("Temp 2 is: ");
  //Serial.println(sensors.getTempC(temps[1]));

  //Serial.print("Temp 3 is: ");
  //Serial.println(sensors.getTempC(temps[2]));
  digitalWrite(LED3, LOW);
  
  timeTaken = millis()-saveTime;
  if (timeTaken < 1000)
  {
      delay(1000-timeTaken);               // wait for a second
  }
  saveTime = millis();
  digitalWrite(LED, LOW);    // turn the LED off by making the voltage LOW

  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
  //lcd.setCursor(1, 1);
  // print the number of seconds since reset:
  //lcd.print(millis()/1000);

  digitalWrite(RF_DATA, HIGH);
  digitalWrite(RF_VCC, HIGH);
  delay(15);
  digitalWrite(RF_DATA, LOW);
  delay(15);
  for (char i=0; i<2; i++)
  {
	sendID();
	chan1();
	if (temp1 > TEMP_OFF) {
       digitalWrite(LED2, LOW);
	   SendOff();
	} else if (temp1 < TEMP_ON) {
       digitalWrite(LED2, HIGH);
	   SendOn();
	}
	DoSync();
  }
  digitalWrite(RF_VCC, LOW);

  timeTaken = millis()-saveTime;
  if (timeTaken < 2000)
  {
      delay(2000-timeTaken);               // wait for a second
  }

}
